# Multi-step-form
A responsive multi-step form with authentication.

### How I walk through this project: 
https://earthy-fuel-f88.notion.site/Multi-Form-Project-808ecc514d184207866da1fb89177d84

### Live:
https://multi-step-form-eta.vercel.app/

## Users

```
Email Address: user1@app.com
Password: user1@app.com
```

## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run serve
```

### Compiles and minifies for production
```
pnpm run build
```

### Lints and fixes files
```
pnpm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
