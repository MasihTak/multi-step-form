import { getAuth, onAuthStateChanged } from 'firebase/auth';
import {
  ref, computed, onMounted, onUnmounted,
} from 'vue';
import { initializeApp } from 'firebase/app';

export const firebaseApp = initializeApp({
  apiKey: 'AIzaSyAZtpgUZydaHrI98KHL8uHesIF3E7Ohwnk',
  authDomain: 'form-1f360.firebaseapp.com',
  projectId: 'form-1f360',
  storageBucket: 'form-1f360.appspot.com',
  messagingSenderId: '370054507203',
  appId: '1:370054507203:web:4f8572a4ca6f3ebbbb9076',
});

export const getUserState = () => new Promise((resolve, reject) => onAuthStateChanged(
  getAuth(), resolve, reject,
));

export const useAuthState = () => {
  const user = ref(null);
  const error = ref(null);

  const auth = getAuth();
  let unsubscribe;
  onMounted(() => {
    unsubscribe = onAuthStateChanged(
      auth,
      // eslint-disable-next-line no-return-assign
      (u) => (user.value = u),
      // eslint-disable-next-line no-return-assign
      (e) => (error.value = e),
    );
  });
  onUnmounted(() => unsubscribe());

  const isAuthenticated = computed(() => user.value != null);

  return { user, error, isAuthenticated };
};
