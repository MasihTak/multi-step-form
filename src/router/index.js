import { createRouter, createWebHistory } from 'vue-router';
// eslint-disable-next-line import/extensions
import { getUserState } from '../../firebase.js';
import Home from '../views/Home.vue';
import Order from '../views/Order.vue';
import Checkout from '../views/Checkout.vue';
import NotFound from '../views/ErrorPages/NotFound.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/order',
    name: 'Order',
    component: Order,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/checkout',
    name: 'Checkout',
    component: Checkout,
    meta: {
      requiresAuth: true,
    },
  },
  {
    // This route must be at the end!
    path: '/:notFound(.*)',
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  linkActiveClass: 'active',
  routes,
});

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  const isAuth = await getUserState();

  if (requiresAuth && !isAuth) {
    next('/');
  } else {
    next();
  }
});

export default router;
